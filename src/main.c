#define _GNU_SOURCE

#include <assert.h>

#include "mem.h"
#include "mem_internals.h"

#define printfn(...) fprintf(stdout, __VA_ARGS__); fprintf(stdout, "\n")

#define ALLOCATION_FAIL "Allocation failed"
#define CAPACITY_FAIL "Capacity failed"
#define CHECK_NEXT_FAIL "Checking next failed"
#define EXTEND_REGION_FAIL "Extending region failed"
#define FREE_FAIL "Free failed"
#define HEADER_FAIL "Header failed"
#define PASSED "PASSED"
#define REGION_FAIL "Region failed"


static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

static char* test_alloc() {
  printfn("Test 1 started: Testing allocation");

  heap_init(0);

  void *addr = _malloc(BLOCK_MIN_CAPACITY * 2);
  assert(addr != NULL && ALLOCATION_FAIL);

  struct block_header* header = block_get_header(addr);
  
  assert(header != NULL && HEADER_FAIL);
  assert(header->capacity.bytes == BLOCK_MIN_CAPACITY * 2 && CAPACITY_FAIL);
  
  void *addr1 = _malloc(BLOCK_MIN_CAPACITY/2);
  assert(addr1 != NULL && ALLOCATION_FAIL);
  struct block_header* header1 = block_get_header(addr1);
  
  assert(header1 != NULL && HEADER_FAIL);
  assert(header1->capacity.bytes == BLOCK_MIN_CAPACITY && CAPACITY_FAIL);
  
  heap_term();
  
  return PASSED;
}

static char* test_free_one() {
  printfn("Test 2 started: Testing free one");
  
  heap_init(0);
  
  void* addr1 = _malloc(BLOCK_MIN_CAPACITY);
  void* addr2 = _malloc(BLOCK_MIN_CAPACITY);
  assert(addr1 != NULL && addr2 != NULL && ALLOCATION_FAIL);
  
  struct block_header* header1 = block_get_header(addr1);  
  struct block_header* header2 = block_get_header(addr2);
  assert(header1 != NULL && header2 != NULL && HEADER_FAIL);
  
  _free(addr2);
  
  assert(header1->next == header2 && CHECK_NEXT_FAIL);
  assert(header2->is_free && FREE_FAIL);
  
  heap_term();
  
  return PASSED;
}

static char* test_free_two() {
  printfn("Test 3 started: Testing free two");

  heap_init(0);  
  
  void* addr1 = _malloc(BLOCK_MIN_CAPACITY);
  void* addr2 = _malloc(BLOCK_MIN_CAPACITY);
  void* addr3 = _malloc(BLOCK_MIN_CAPACITY);
  assert(addr1 != NULL && addr2 != NULL && addr3 != NULL && ALLOCATION_FAIL);
  
  struct block_header* header1 = block_get_header(addr1);  
  struct block_header* header2 = block_get_header(addr2);  
  struct block_header* header3 = block_get_header(addr3);  
  assert(header1 != NULL && header2 != NULL && header3 != NULL && HEADER_FAIL);
  
  _free(addr3);
  _free(addr2);
  
  assert(!header1->is_free && FREE_FAIL);
  assert(header2->is_free && FREE_FAIL);
  assert(header3->is_free && FREE_FAIL);
  assert(header1->next == header2 && CHECK_NEXT_FAIL);
  assert(header2->next == NULL && CHECK_NEXT_FAIL);
  
  heap_term();  
    
  return PASSED;
}

static char* test_extend_region() {
  printfn("Test 4 started: Testing extending region");
  
  heap_init(0);
  
  void* addr1 = _malloc(3 * REGION_MIN_SIZE);
  void* addr2 = _malloc(3 * REGION_MIN_SIZE);
  assert(addr1 != NULL && addr2 != NULL && ALLOCATION_FAIL);
  
  struct block_header* header1 = block_get_header(addr1);
  struct block_header* header2 = block_get_header(addr2);
  assert(header1 != NULL && header2 != NULL && HEADER_FAIL);

  assert(header1->next == header2 && CHECK_NEXT_FAIL);
  assert((void*)(header1->contents + header1->capacity.bytes) == header2 && EXTEND_REGION_FAIL);
  
  heap_term();
  
  return PASSED;
}

static char* test_region_in_new_place() {
  printfn("Test 5 started: Testing region in new place");
  
  heap_init(0);
  
  void* region = mmap(HEAP_START + REGION_MIN_SIZE, 100, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  assert(region != NULL && ALLOCATION_FAIL);
  void* addr = _malloc(capacity_from_size((block_size) { REGION_MIN_SIZE }).bytes);
  assert(addr != NULL && ALLOCATION_FAIL);

  struct block_header* header = block_get_header(addr);
  assert(header != NULL && HEADER_FAIL);
  
  assert(region != addr && REGION_FAIL); 
  
  heap_term();
  
  return PASSED;
}

int main() {
  printfn("Tests begin:");
  printfn("TEST1 : %s", test_alloc());
  printfn("TEST2 : %s", test_free_one());
  printfn("TEST3 : %s", test_free_two());
  printfn("TEST4 : %s", test_extend_region());
  printfn("TEST5 : %s", test_region_in_new_place());
  printfn("All tests successfully passed!");
  return 0;
}
